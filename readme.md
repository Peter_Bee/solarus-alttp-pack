# Zelda ALTTP Resource Pack for Solarus

![A Link To The Past logo](data/logos/logo.png)

This repository provides musics, sounds, tilesets, sprites and scripts
from the 1992 Super Nintendo game *The Legend of Zelda: A Link to the Past*,
adapted for the
[Solarus engine](https://github.com/solarus-games/solarus).

You can use these resources if you want to develop a game with the
Solarus engine using ALTTP graphics.

## Goal

One goal of this resource pack is also to provide at least all
[data files required by Solarus](https://github.com/solarus-games/solarus/blob/master/work/data_files.txt).

Since *A Link to the Past* is huge,
there will always be missing or incomplete elements.
Feel free to contribute!

Also, there may be unwanted elements,
typically, resources that don't come from ALTTP
but that were created for *Mystery of Solarus DX*
and ended up here.
If you find such inconsistencies, please report them by
opening an issue.

## Branches

Branch master always points to the latest release of this resource pack.

The latest resource pack release is always compatible
with the latest Solarus version.

Resources compatible with older versions or development versions of Solarus
live in their own branches.

## Create a new quest with ALTTP resources

Using the ALTTP resource pack for a new quest is straightforward.
The idea is to create your quest as a copy of the ALTTP resource pack
rather than creating it from the quest editor:

- Create a new empty folder for your quest.
- Copy the `data` folder of the ALTTP resource pack and all its content
  into your quest's folder.
- You can now open your new quest with
  [Solarus Quest Editor](http://www.solarus-games.org/development/quest-editor]).
- Edit the quest properties (Ctrl+P) to set a title, a write directory
  and other information of your game.

## Integrate ALTTP resources into an existing quest

Importing ALTTP resorces into your existing game is now a lot easier.

But remember, always make a backup of your quest before adding any resources.

- Open your quest in
  [Solarus Quest Editor](http://www.solarus-games.org/development/quest-editor]).
- In the File menu select 'Import from a quest...' (Ctrl+I)
- For the Source quest, navigate to and select the `solarus-alttp-pack-dev` 
  directory and click on 'Select folder'
  Click on any files, scripts, images or folders that you want for your 
  quest to select them and click on 'Import .x. items'.
- If you don't want the whole folder but only a few sprites, tilesets or 
  sounds, you can only pick the resources you need. Shift and Ctrl work as 
  normal if you want to select multiple resources.
- In your quest tree, all resources you just copied now appear in the same 
  location that they were in the source quest.
- The 'Identify missing' button will show how many flies are 'missing' in 
  your quest compared to the `solarus-alttp-pack-dev` source quest. The button 
  is only functional if you select a folder in the Source quest tree. It 
  will work on any or multiple folders, irrespective if they main or 
  sub-folders. The files are automatically selected, just click on 
  'Import .x. items' to import them into your quest.
- If a resource of the same name already exists in your quest, a warning will 
  be displayed giving you the option to overwrite, skip or rename the resource.
  Be careful though, if you overwrite the resource in your quest then the 
  original file will have gone. 
- This may cause issues for other linked files. For instance if you overwrite 
  your tilesets, maps may look different if any tiles on the map are not on 
  your new tileset.

